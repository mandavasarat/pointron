#!/usr/bin/env node
import * as route53 from "aws-cdk-lib/aws-route53";
import * as s3 from "aws-cdk-lib/aws-s3";
import * as acm from "aws-cdk-lib/aws-certificatemanager";
import * as cloudfront from "aws-cdk-lib/aws-cloudfront";
import * as s3deploy from "aws-cdk-lib/aws-s3-deployment";
import * as targets from "aws-cdk-lib/aws-route53-targets";
import * as cloudfront_origins from "aws-cdk-lib/aws-cloudfront-origins";
import { CfnOutput, Duration, RemovalPolicy, Stack } from "aws-cdk-lib";
import * as iam from "aws-cdk-lib/aws-iam";
import { Construct } from "constructs";

export interface StaticSiteProps {
  domainName: string;
  siteSubDomain: string[];
}

export class StaticSite extends Construct {
  constructor(parent: Stack, name: string, props: StaticSiteProps) {
    super(parent, name);
    const siteDomain = props.siteSubDomain[0] + "." + props.domainName;
    const zone = route53.HostedZone.fromLookup(this, "Zone", {
      // domainName: props.domainName
      domainName: siteDomain
    });
    // const zone = route53.HostedZone.fromHostedZoneAttributes(
    //   this,
    //   "HostedZone",
    //   {
    //     hostedZoneId: "Z00588642C0WAWY2GCF9J",
    //     zoneName: props.domainName
    //   }
    // );
    // const allSubDomains = props.siteSubDomain.map(
    //   (subDomain) => subDomain + "." + props.domainName
    // ) as string[];
    // console.log({ siteDomain, allSubDomains });
    const cloudfrontOAI = new cloudfront.OriginAccessIdentity(
      this,
      "cloudfront-OAI",
      {
        comment: `OAI for ${name}`
      }
    );

    new CfnOutput(this, "Site", { value: "https://" + siteDomain });

    const siteBucket = new s3.Bucket(this, "SiteBucket", {
      bucketName: siteDomain,
      publicReadAccess: false,
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      removalPolicy: RemovalPolicy.DESTROY, //TODO - check if needed for production
      autoDeleteObjects: true
    });

    siteBucket.addToResourcePolicy(
      new iam.PolicyStatement({
        actions: ["s3:GetObject"],
        resources: [siteBucket.arnForObjects("*")],
        principals: [
          new iam.CanonicalUserPrincipal(
            cloudfrontOAI.cloudFrontOriginAccessIdentityS3CanonicalUserId
          )
        ]
      })
    );
    new CfnOutput(this, "Bucket", { value: siteBucket.bucketName });

    const certificate = new acm.Certificate(this, "SiteCertificate", {
      // domainName: "*." + props.domainName,
      domainName: siteDomain,
      validation: acm.CertificateValidation.fromDns(zone)
    });
    // const certificateArn =
    //   "arn:aws:acm:us-east-1:157103514804:certificate/50f3aaf3-0a48-4df6-8436-afc22a66531e";

    // const certificate = acm.Certificate.fromCertificateArn(
    //   this,
    //   "SiteCertificate",
    //   certificateArn
    // );

    new CfnOutput(this, "Certificate", { value: certificate.certificateArn });

    const distribution = new cloudfront.Distribution(this, "SiteDistribution", {
      certificate: certificate,
      defaultRootObject: "index.html",
      // domainNames: [...allSubDomains],
      domainNames: [siteDomain],
      minimumProtocolVersion: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2021,
      errorResponses: [
        {
          httpStatus: 403,
          responseHttpStatus: 403,
          responsePagePath: "/index.html",
          ttl: Duration.minutes(30)
        }
      ],
      defaultBehavior: {
        origin: new cloudfront_origins.S3Origin(siteBucket, {
          originAccessIdentity: cloudfrontOAI
        }),
        compress: true,
        allowedMethods: cloudfront.AllowedMethods.ALLOW_GET_HEAD_OPTIONS,
        viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS
      }
    });

    new CfnOutput(this, "DistributionId", {
      value: distribution.distributionId
    });

    const recordTarget = route53.RecordTarget.fromAlias(
      new targets.CloudFrontTarget(distribution)
    );
    // allSubDomains.forEach((sub, index) => {
    //   new route53.ARecord(this, `PointronARecord${index}`, {
    //     recordName: sub,
    //     target: recordTarget,
    //     zone
    //   });
    // });
    new route53.ARecord(this, `FrontEndARecord`, {
      recordName: siteDomain,
      target: recordTarget,
      zone
    });

    new s3deploy.BucketDeployment(this, "DeployWithInvalidation", {
      sources: [s3deploy.Source.asset("./../build")],
      destinationBucket: siteBucket,
      distribution,
      distributionPaths: ["/*"]
    });
  }
}
