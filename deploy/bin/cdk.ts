#!/usr/bin/env node
import * as cdk from "aws-cdk-lib";
import { ClientStack } from "../lib/tidydeploy/clientStack";

const domain = process.env.domain ?? "pointron.io";
const appName = domain.split(".")[0];
const subdomain = process.env.subdomain ?? "dev";
console.log("FrontendStack", {
  domain,
  subdomain
});
class FrontendStack extends cdk.Stack {
  constructor(parent: cdk.App, name: string, props: cdk.StackProps) {
    super(parent, name, props);
    new ClientStack(this, `${appName}Frontend`, {
      domain,
      subdomain
    });
  }
}

const app = new cdk.App();

new FrontendStack(app, `${appName}FrontendStack`, {
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT ?? process.env.account,
    region: "us-east-1"
  }
});
