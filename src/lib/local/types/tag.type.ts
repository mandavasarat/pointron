import type { DbRecordBase } from "$lib/tidy/types/dbrecord.type";
import type { CacheableStore } from "$lib/tidy/types/data.type";

export type Tag = {
  id: string;
  label: string;
  hue?: number;
};

export type PointTagDbType = DbRecordBase & {
  label: string;
};

export interface TagStore extends CacheableStore {
  tags: Tag[];
}
