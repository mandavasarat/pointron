import type { Placement } from "$lib/tidy/types/placement.type";
import type { SelectionItemActiveStyle } from "$lib/tidy/types/switcher.enum";

export type NavigationMenuItem = {
  label: string;
  link: string;
  icon?: string;
  iconPlacement?: Placement.LEFT | Placement.RIGHT;
  style?: SelectionItemActiveStyle;
};
