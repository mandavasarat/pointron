import type {
  SessionComposition,
  SessionCompositionType
} from "$lib/local/types/sessionComposition.type";
import type { TimerMode } from "$lib/local/types/timerMode.enum";
import type { HorizonChart } from "$lib/tidy/types/analytics.type";
import type { Cloud } from "$lib/tidy/types/cloud.enum";
import type { Layout } from "$lib/tidy/types/layout.type";
import type { CacheableStore } from "$lib/tidy/types/data.type";
import type { TimeScale } from "$lib/tidy/types/time.type";
import type { AnalyticsFilters } from "./analytics.type";

export interface UserLocalPreferences extends CacheableStore {
  isEnableAgeCounter: boolean;
  breakEndSound?: string;
  focusEndSound?: string;
  sessionFinishSound?: string;
  extendDuration: number;
  presets: SessionComposition[];
  isEnableAutoStartInterval: boolean;
  isIncludeBreakInAnalytics: boolean;
  timerMode: TimerMode;
  breakReminder: number;
  appMenu: string[];
  cloudProvider?: Cloud;
  manualEntryQuickDurations?: number[];
  horizonCharts: HorizonChart[];
  horizonChartFilters?: AnalyticsFilters;
  horizonsWithTarget?: TimeScale[];
  horizonTargets?: { scale: TimeScale; target: number }[];
  uiStates: {
    all: LocalUiState;
    desktop: LocalUiState;
    portrait: LocalUiState;
  };
}

type LocalUiState = {
  quickFocusLayout: Layout;
  advancedComposeType: SessionCompositionType;
  advancedMode: number;
};
