export type LandingAboutProductSlides = {
  portrait: { title: string; description: string; image: string }[];
  landscape: { title: string; description: string; image: string }[];
};
