export enum localUiStateEnum {
  quickFocusLayout = "quickFocusLayout",
  advancedComposeType = "advancedComposeType",
  advancedMode = "advancedMode",
}
