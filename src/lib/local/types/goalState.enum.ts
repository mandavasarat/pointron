export enum GoalState {
  SAVED = "SAVED",
  UNSAVED = "UNSAVED",
  NEW = "NEW",
}
