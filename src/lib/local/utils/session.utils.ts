import { todayFocusStore } from "../stores/session.store";
import { AnalyticsPersistance } from "../stores/analytics.persistance";
import { TimeScale } from "$lib/tidy/types/time.type";
import { sortArrayByOrder } from "$lib/tidy/utils/obj.utils";
import type { FocusItem } from "../types/session.type";

export async function refreshTodayFocus() {
  let todayFocus =
    await new AnalyticsPersistance().fetchAggFocusByCurrentTimeHorizon(
      TimeScale.DAYS
    );
  todayFocusStore.set({ focus: todayFocus, streak: 0 });
}

export function transformFocusItems(rawItems: FocusItem[]) {
  let items: any[] = [];
  rawItems.forEach((item: FocusItem) => {
    if (item.goalId && !item.taskId) {
      let tasks = rawItems.filter(
        (x: FocusItem) => x.goalId === item.goalId && x.taskId
      );
      if (tasks && tasks.length > 0) {
        tasks = sortArrayByOrder(tasks);
        tasks = tasks.map((x: FocusItem) => {
          x.color = item.color;
          return x;
        });
        items = items.concat({ ...item, tasks });
      } else items = items.concat(item);
    } else if (!item.goalId && item.taskId) {
      items = items.concat(item);
    }
  });
  return sortArrayByOrder(items);
}
